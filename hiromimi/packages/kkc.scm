;;; Forked version: by hiromi-mi
;;; License: GPLv3
(define-module (hiromimi packages kkc)
#:use-module (guix packages)
#:use-module (guix download)
#:use-module (guix build utils) ;; modify-phases
#:use-module (guix build-system python)
#:use-module ((guix build-system gnu) #:prefix gnu:)
#:use-module (guix licenses)
#:use-module (guix gexp)
#:use-module (ice-9 ftw) ;; filetree
#:use-module (gnu packages check)
#:use-module (gnu packages python)
#:use-module (gnu packages datastructures)
#:use-module (gnu packages gnome)
#:use-module ((gnu packages base) #:prefix base:)
#:use-module (gnu packages xdisorg)
#:use-module (gnu packages ibus)
#:use-module (gnu packages glib)
#:use-module (gnu packages gtk)
#:use-module (gnu packages autotools)
#:use-module (gnu packages gettext)
#:use-module (gnu packages pkg-config)
	     )


;;; Original Version (on marisa: guix: gnu/packages/datastructures.scm )
;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2015, 2016, 2018, 2019 Ricardo Wurmus <rekado@elephly.net>
;;; Copyright © 2016, 2017, 2019, 2020 Tobias Geerinckx-Rice <me@tobias.gr>
;;; Copyright © 2018 Meiyo Peng <meiyo.peng@gmail.com>
;;; Copyright © 2019 Efraim Flashner <efraim@flashner.co.il>
;;; Copyright © 2020 Mark H Weaver <mhw@netris.org>

(define-public python-marisa
  (package
    (name "python-marisa")
    (version "0.2.6")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/s-yata/marisa-trie/files/"
                           "4832504/marisa-" version ".tar.gz"))
       (sha256
        (base32 "1pk6wmi28pa8srb4szybrwfn71jldb61c5vgxsiayxcyg1ya4qqh"))))
    (build-system python-build-system)
    (arguments
      `(#:phases
      (modify-phases %standard-phases
		     (add-after 'unpack 'chdir
				(lambda _ (chdir "bindings/python") #t)))))
    (inputs
      `(
	("marisa", marisa)))
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)))
    (home-page "https://github.com/s-yata/marisa-trie")
    (synopsis "Trie data structure C++ library for python binding")
    (description "@acronym{MARISA, Matching Algorithm with Recursively
Implemented StorAge} is a static and space-efficient trie data structure C++
library.")

    ;; Dual-licensed, according to docs/readme.en.html (source files lack
    ;; copyright/license headers.)
    (license (list bsd-2 lgpl2.1+))))
;;; end derivation


(define-public python2-marisa
	       (package-with-python2 python-marisa))

(define-public python-marisa-trie
(package
  (name "python-marisa-trie")
  (version "0.7.5")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "marisa-trie" version))
      (sha256
        (base32
          "1brbxc3gmk20afq6j4imccjblyxh5n4ij7kzmaklx34fhrfw4fy7"))))
  (build-system python-build-system)
  ;; from check.scm
  (arguments '(#:tests? #f))
  (home-page
    "https://github.com/kmike/marisa-trie")
  (synopsis
    "Static memory-efficient and fast Trie-like structures for Python.")
  (description
    "Static memory-efficient and fast Trie-like structures for Python.")
  (native-inputs
    `(("python-pytest-runner", python-pytest-runner)
      ("python-hypothesis" ,python-hypothesis)
      ("python-pytest" ,python-pytest)))
  (license mit))
)

(define-public libkkc
(package
  (name "libkkc")
  (version "0.3.5")
  (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ueno/libkkc/archive/d7af4c1e5beedbf0ad6689457972f790725dd71b.tar.gz"))
	    (sha256
	      (base32
		"0m5i4rbnxy6qy83wciq07j8h83vx44gpdh8gvisi5r22shcg9h8x"))))

  (build-system gnu:gnu-build-system)
  (synopsis "Japanese Kana Kanji conversion input method library")
  (description "a library to deal with Japanese kana-to-kanji conversion method")
  (home-page "https://github.com/ueno/libkkc")
  (license gpl3+)
  (inputs
    `(("libgee",libgee)
      ("glib",glib) ; for gio-2.0
      ("json-glib",json-glib)
      ("libxkbcommon",libxkbcommon)
      ("valac", vala)
      ("python2", python-2.7)
      ("gettext", gnu-gettext)
      ("marisa", marisa)
      ("python2-marisa", python2-marisa)
      ("gobject-introspection", gobject-introspection)))
  (native-inputs
    `(("pkg-config",pkg-config)
      ("autoconf", autoconf)
      ("automake", automake)
      ("libtool", libtool)
      ))))

(define-public ibus-kkc
(package
  (name "ibus-kkc")
  (version "1.5.22")
  (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ueno/ibus-kkc/archive/master.tar.gz"))
	    (sha256
	      (base32
		"1xfz25pirnhwjbv434mwgwh0niw27f3wz6y828iblgsv85y9fbsy"))))

  (build-system gnu:gnu-build-system)
  (synopsis "using KKC with Ibus")
  (description "using KKC with Ibus")
  (home-page "https://github.com/ueno/ibus-kcc")
  (license gpl3+)
  (inputs
    `(("ibus",ibus)
      ("libgee",libgee) ;;; required by libskk
      ("glib",glib) ; for gio-2.0
      ("glib:bin",glib "bin") ; for glib-compile-resources
      ("gtk+",gtk+)
      ("libkkc",libkkc)
      ("gnome-common",gnome-common)
      ("which",base:which)
      ("json-glib",json-glib)
      ("marisa", marisa)
      ("gobject-introspection", gobject-introspection)
      ))
  (native-inputs
    `(("intltool",intltool)
      ("autoconf", autoconf)
      ("valac", vala)
      ("automake", automake)
      ("python2-marisa", python2-marisa)
      ("libtool", libtool)
      ("python2", python-2.7)
      ("pkg-config",pkg-config))))
)

(define-public libkkc-data
(package
  (name "libkkc-data")
  (version "0.2.7")
  (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ueno/libkkc/releases/download/v0.3.5/libkkc-data-" version ".tar.xz"))
	    (sha256
	      (base32
		"16avb50jasq2f1n9xyziky39dhlnlad0991pisk3s11hl1aqfrwy"))))

  (build-system gnu:gnu-build-system)
  (synopsis "Kanji Kana Input model: data")
  (description "Kanji Kana Input model: data")
  (home-page "https://github.com/ueno/libkkc")
  (license gpl3+)
  (inputs
    `(
     ;; ("libgee",libgee) ;;; required by libskk
     ;; ("gtk+",gtk+)
     ;; ("libkkc",libkkc)
     ;; ("json-glib",json-glib)
      ("marisa", marisa)
     ;; ("gobject-introspection", gobject-introspection)
      ))
  (native-inputs
    `(("intltool",intltool)
      ("autoconf", autoconf)
      ("automake", automake)
      ("python2-marisa", python2-marisa)
      ("libtool", libtool)
      ("python2", python-2.7)
      ("pkg-config",pkg-config))))
)
