;;; Copyright 2020 hiromi_mi hiromi-mi at cat.zaq.jp
;;; SPDX-License-Identifier: GPLv3

(define-module (hiromimi packages skk)
#:use-module (guix packages)
#:use-module (guix download)
#:use-module (guix build-system gnu)
#:use-module (guix licenses)
#:use-module (gnu packages gnome)
#:use-module (gnu packages xdisorg)
#:use-module (gnu packages glib)
#:use-module (gnu packages gtk)
#:use-module (gnu packages ibus)
#:use-module (gnu packages pkg-config))

(define-public libskk
(package
  (name "libskk")
  (version "1.0.5")
  (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ueno/libskk/releases/download/" version "/libskk-" version ".tar.xz"))
	    (sha256
	      (base32
		"0j9rkjh3pxsnzwwr9ivdzhj7aqjfbjsv4x34r583fdcf4jffp5dj"))))

  (build-system gnu-build-system)
  (synopsis "Japanese SKK input method library")
  (description "a library to deal with Japanese kana-to-kanji conversion method")
  (home-page "https://github.com/ueno/libskk")
  (license gpl2+)
  (inputs
    `(("libgee",libgee)
      ("glib",glib) ; for gio-2.0
      ("json-glib",json-glib)
      ("libxkbcommon",libxkbcommon)))
  (native-inputs
    `(("pkg-config",pkg-config)))))

(define-public ibus-skk
(package
  (name "ibus-skk")
  (version "1.4.3")
  (source (origin
	    (method url-fetch)
	    (uri (string-append "https://github.com/ueno/ibus-skk/releases/download/ibus-skk-" version "/ibus-skk-" version ".tar.xz"))
	    (sha256
	      (base32
		"0pb5766njjsv0cfikm0j5sq0kv07sd5mlxj1c06k5y6p1ffvsqb6"))))

  (build-system gnu-build-system)
  (synopsis "a Japanese SKK input engine for IBus")
  (description "an implementation of the SKK (Simple Kana-Kanji) input method on the IBus input method framework")
  (home-page "https://github.com/ueno/ibus-skk")
  (license gpl2+)
  (inputs
    `(("ibus",ibus)
      ("libgee",libgee) ;;; required by libskk
      ("gtk+",gtk+)
      ("libskk",libskk)))
  (native-inputs
    `(("intltool",intltool)
      ("vala", vala)
      ("pkg-config",pkg-config))))
)
