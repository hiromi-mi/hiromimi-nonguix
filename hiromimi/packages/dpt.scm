;;; Copyright 2020 hiromi_mi hiromi-mi at cat.zaq.jp
;;; SPDX-License-Identifier: GPLv3
(define-module (hiromimi packages dpt)
#:use-module (guix packages)
#:use-module (guix download)
#:use-module (guix build-system python)
#:use-module ((guix build-system gnu) #:prefix gnu:)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (guix gexp)
#:use-module (gnu packages python-web)
#:use-module (gnu packages python-crypto)
#:use-module (gnu packages check)
#:use-module (gnu packages python)
#:use-module (gnu packages datastructures)
#:use-module (gnu packages gnome)
#:use-module ((gnu packages base) #:prefix base:)
#:use-module (gnu packages xdisorg)
#:use-module (gnu packages python-xyz)
#:use-module (gnu packages glib)
#:use-module (gnu packages gtk)
#:use-module (gnu packages autotools)
#:use-module (gnu packages gettext)
#:use-module (gnu packages pkg-config)
	     )
(define-public python-ifaddr
  (package
    (name "python-ifaddr")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ifaddr" version))
        (sha256
          (base32
            "150sxdlicwrphmhnv03ykxplyd2jdrxz0mikgnivavgilrn8m7hz"))))
    (build-system python-build-system)
    (home-page "https://github.com/pydron/ifaddr")
    (synopsis
      "Cross-platform network interface and IP address enumeration library")
    (description
      "Cross-platform network interface and IP address enumeration library")
    (arguments '(#:tests? #f))
    (license license:mit)))

(define-public python-zeroconf
  (package
    (name "python-zeroconf")
    (version "0.28.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "zeroconf" version))
        (sha256
          (base32
            "00vs4l7p15isaprx7n6484ckad1inl18pk5ikyshm3kw7pns47c8"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-ifaddr" ,python-ifaddr)))
    (home-page
      "https://github.com/jstasiak/python-zeroconf")
    (synopsis
      "Pure Python Multicast DNS Service Discovery Library (Bonjour/Avahi compatible)")
    (description
      "Pure Python Multicast DNS Service Discovery Library (Bonjour/Avahi compatible)")
    (arguments '(#:tests? #f))
    (license license:lgpl2.1)))

(define-public python-httpsig
  (package
    (name "python-httpsig")
    (version "1.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "httpsig" version))
        (sha256
          (base32
            "1rkc3zwsq53rjsmc47335m4viljiwdbmw3y2zry4z70j8q1dbmki"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-pycryptodome" ,python-pycryptodome)
	("python-setuptools-scm", python-setuptools-scm)
        ("python-six" ,python-six)))
    (home-page "https://github.com/ahknight/httpsig")
    (synopsis
      "Secure HTTP request signing using the HTTP Signature draft specification")
    (description
      "Secure HTTP request signing using the HTTP Signature draft specification")
    (license license:mit)))

(define-public python-dpt-rp1-py
  (package
    (name "python-dpt-rp1-py")
    (version "0.1.11")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "dpt-rp1-py" version))
        (sha256
          (base32
            "0jy9fvmb6a3fcnijxk6xnss3k4c9pjffggr006xsvbig1lprcx5r"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-anytree" ,python-anytree)
        ("python-fusepy" ,python-fusepy)
        ("python-httpsig" ,python-httpsig)
        ("python-pbkdf2" ,python-pbkdf2)
        ("python-pyyaml" ,python-pyyaml)
        ("python-requests" ,python-requests)
        ("python-tqdm" ,python-tqdm)
        ("python-urllib3" ,python-urllib3)
        ("python-zeroconf" ,python-zeroconf)))
    (home-page "https://github.com/janten/dpt-rp1-py")
    (arguments '(#:tests? #f))
    (synopsis
      "Python package to manage a Sony DPT-RP1")
    (description
      "Python package to manage a Sony DPT-RP1")
    (license license:mit)))
